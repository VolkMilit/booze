import os
import time
import struct
from io import BytesIO
from pathlib import Path

import pefile
from PIL import Image

# Original class by 57ur14 (https://github.com/57ur14/pefile-extract-icon)
class ExtractIcon(object):
    GRPICONDIRENTRY_format = ('GRPICONDIRENTRY',
        ('B,Width', 'B,Height','B,ColorCount','B,Reserved',
        'H,Planes','H,BitCount','I,BytesInRes','H,ID'))
    GRPICONDIR_format = ('GRPICONDIR',
        ('H,Reserved', 'H,Type','H,Count'))

    def __init__(self, pe_object):
        self.pe = pe_object

    def find_resource_base(self, type):
        rt_base_idx = None
        try:
            rt_base_idx = [entry.id for
                entry in self.pe.DIRECTORY_ENTRY_RESOURCE.entries].index(
                    pefile.RESOURCE_TYPE[type]
            )
        except Exception:
            pass

        if rt_base_idx is not None:
            return self.pe.DIRECTORY_ENTRY_RESOURCE.entries[rt_base_idx]

        return None

    def find_resource(self, type, res_index):
        rt_base_dir = self.find_resource_base(type)

        if res_index < 0:
            try:
                idx = [entry.id for entry in rt_base_dir.directory.entries].index(-res_index)
            except Exception:
                return None
        else:
            try:
                idx = res_index if res_index < len(rt_base_dir.directory.entries) else None
            except AttributeError:
                idx = None

        if idx is None: 
            return None

        test_res_dir = rt_base_dir.directory.entries[idx]
        res_dir = test_res_dir
        if test_res_dir.struct.DataIsDirectory:
            # another Directory
            # probably language take the first one
            try:
                res_dir = test_res_dir.directory.entries[0]
            except IndexError:
                return None
        if res_dir.struct.DataIsDirectory:
            # Ooooooooooiconoo no !! another Directory !!!
            return None

        return res_dir

    def get_group_icons(self):
        rt_base_dir = self.find_resource_base('RT_GROUP_ICON')
        if rt_base_dir is None:
            return None
        groups = list()
        for res_index in range(0, len(rt_base_dir.directory.entries)):
            grp_icon_dir_entry = self.find_resource('RT_GROUP_ICON', res_index)

            if not grp_icon_dir_entry:
                continue

            data_rva = grp_icon_dir_entry.data.struct.OffsetToData
            size = grp_icon_dir_entry.data.struct.Size
            data = self.pe.get_memory_mapped_image()[data_rva:data_rva+size]
            if len(data) == 0:
                continue
            file_offset = self.pe.get_offset_from_rva(data_rva)

            grp_icon_dir = pefile.Structure(self.GRPICONDIR_format, file_offset=file_offset)
            grp_icon_dir.__unpack__(data)

            if grp_icon_dir.Reserved != 0 or grp_icon_dir.Type != 1:
                continue
            offset = grp_icon_dir.sizeof()

            entries = list()
            for idx in range(0, grp_icon_dir.Count):
                grp_icon = pefile.Structure(self.GRPICONDIRENTRY_format, file_offset=file_offset+offset)
                try:
                    grp_icon.__unpack__(data[offset:])
                except pefile.PEFormatError:
                    return None
                offset += grp_icon.sizeof()
                entries.append(grp_icon)

            groups.append(entries)
        return groups

    def best_icon(self, entries: list) -> list:
        b = 0
        w = 0
        best = None
        for i in range(len(entries)):
            icon = entries[i]
            if icon.BitCount > b:
                b = icon.BitCount
                best = i
            if icon.Width > w and icon.BitCount == b:
                w = icon.Width
                b = icon.BitCount
                best = i
        return best

    def get_windows_preferred_icon(self):
        """
        Identify the group and the index of the icon windows normally would display.
        Returns the group and the icon index. These values can be used as input for
        other functions in this class to retrieve the icon itself.
        """
        group_icons = self.get_group_icons()
        if group_icons is not None:
            best_icon = 0
            for group in group_icons:
                if len(group) == 0:
                    continue
                best_icon = self.best_icon(group)
                return group, best_icon
        return None, None

    def get_raw_windows_preferred_icon(self):
        """
        Retrieve the (raw) icon that Windows normally would display.
        Returns the icon if a suitable icon was found.
        If no suitable icon was found, None is returned.
        """
        group, icon = self.get_windows_preferred_icon()
        if group is not None and icon is not None:
            return self.export_raw(group, icon)
        else:
            return None

    def get_icon(self, index):
        icon_entry = self.find_resource('RT_ICON', -index)
        if not icon_entry:
            return None

        data_rva = icon_entry.data.struct.OffsetToData
        size = icon_entry.data.struct.Size
        data = self.pe.get_memory_mapped_image()[data_rva:data_rva+size]

        return data

    def export_raw(self, entries, index = None) -> bytes:
        if index is not None:
            entries = entries[index:index+1]

        ico = struct.pack('<HHH', 0, 1, len(entries))
        data_offset = None
        data = []
        info = []
        for grp_icon in entries:
            if data_offset is None:
                data_offset = len(ico) + ((grp_icon.sizeof()+2) * len(entries))

            nfo = grp_icon.__pack__()[:-2] + struct.pack('<L', data_offset)
            info.append( nfo )

            raw_data = self.get_icon(grp_icon.ID)
            if not raw_data: 
                continue

            data.append( raw_data )
            data_offset += len(raw_data)

        raw = ico + b''.join(info) + b''.join(data)
        return raw

class PElib:
    def __init__(self, exe_path: str):
        self.is_open = True
        self.__exe_path = Path(exe_path)
        
        try:
            self.__pe = pefile.PE(exe_path, fast_load = True)
            self.__pe.parse_data_directories(directories = [
                pefile.DIRECTORY_ENTRY["IMAGE_DIRECTORY_ENTRY_IMPORT"],
                pefile.DIRECTORY_ENTRY["IMAGE_DIRECTORY_ENTRY_RESOURCE"]
            ])
            
            self.__features = {
                "steam_api.dll": "Is Steam game",
                "steam_api64.dll": "Is Steam game",
                "d3dx9_42.dll": "Requires d3dx9_42.dll, wine's probably garbage",
                "d3dx9_43.dll": "Requires d3dx9_43.dll, wine's probably garbage",
                "d3dx9_30.dll": "Requires d3dx9_30.dll, wine's probably garbage",
                "d3dx11_43.dll": "Requires d3dx11_43.dll, wine's probably garbage",
                "dinput8.dll": "Generic gamepads support (dinput)",
                "xinput1_3.dll": "XBox gamepads support",
                "xinput9_1_0.dll": "XBox gamepads support",
                "d3d9.dll": "DirectX 9 support",
                "d3d11.dll": "DirectX 11 support",
                "d3d12.dll": "DirectX 12 support",
                "opengl32.dll": "OpenGL support",
                "vulkan-1.dll": "Vulkan support",
                "msvcp140.dll": "Require VC++14",
                "x3daudio1_7.dll": "Requires faudio",
                "msvcrt.dll": "Require .NET framework",
                "dwmapi.dll": "Require Windows Vista and above",
                "denuvo64.dll": "Denuvo build in and nailed to exe, thanks for this",
                "crypt32.dll": "Require mannual installation of crypt32 and msasn1",
                "setupapi.dll": "Probably installs driver in your prefix, be careful",
                "mscoree.dll": "DotNet application"
            }
        except pefile.PEFormatError:
            print("booze: Unable to extract exe information")
            self.is_open = False
        
    def __del__(self):
        if self.is_open:
            self.__pe.close()
    
    def __imports(self):
        imports_dlls = []
        
        for i in self.__pe.DIRECTORY_ENTRY_IMPORT:
            imports_dlls.append(i.dll.lower().decode())
        
        return imports_dlls
    
    def __build_time(self):
        t = self.__pe.FILE_HEADER.TimeDateStamp
        
        if t == 0:
            return "Unknown"
        
        return time.ctime(t)
    
    def __get_attrs(self, found: str):
        if hasattr(self.__pe, "FileInfo"):
            for i in range(0, len(self.__pe.FileInfo[0])):
                if hasattr(self.__pe.FileInfo[0][i], "StringTable"):
                    table = self.__pe.FileInfo[0][i].StringTable[0].entries.get(found.encode())
                    if table is not None:
                        return table.decode()
        
        return None
    
    def extract_icon(self):
        if self.is_open:
            try:
                icon_extractor = ExtractIcon(self.__pe)
                raw = icon_extractor.get_raw_windows_preferred_icon()

                stream = BytesIO(raw)
                image = Image.open(stream)
                stream.close()

                icons_path = Path.home() / Path(".cache/booze/icon")
                image.save(str(icons_path) + '/' + self.__exe_path.stem + ".png")
            except Exception:
                pass
            
    def name(self):
        if self.is_open:
            return self.__get_attrs("FileDescription") or self.__exe_path.name
        
        return self.__exe_path.name
    
    def is_i386(self):
        return self.__pe.FILE_HEADER.IMAGE_FILE_32BIT_MACHINE
    
    def laa(self):
        return self.__pe.FILE_HEADER.IMAGE_FILE_LARGE_ADDRESS_AWARE
        
    def make_report(self):
        if self.is_open:
            system_dir = "syswow64"
                    
            print("App name: " + self.name())
            print()
            print("Build time is: {}".format(self.__build_time()))
            print()
            
            if self.is_i386() and not self.laa():
                system_dir = "system32"
                print("App is 32 bit. Expect out of memory errors with DXVK\D9VK.")
            
            print()
            
            depends = self.__imports()
            dependencies = "Dependencies (all dlls):\n"
            features = "Features:\n"
            external = "External (unknown) dlls:\n"
            internal = "Internal dlls:\n"
            
            # Special case, there is some games that separate UnityPlayer from binary
            # The problem is: nobody can stop developer from add DX9 or DX11 support
            # So we don't know if there is DX support at all, I don't know how this works
            # but unityplayer only linked with opengl32.dll nor d3d9.dll nor d3d11.dll, but game
            # launches in DX mode, even with -force-opengl. As a example: Risc of Rain 2
            if "unityplayer.dll" in depends:
                print("Generic gamepads support (dinput8)")
                print("XBox gamepads support")
                print("Possible to launch with OpenGL")
                return
            
            if len(depends) > 0:
                for dll in depends:
                    dependencies += dll + "\n"
                    
                    if self.__features.get(dll):
                        features += self.__features.get(dll) + "\n"
                    elif not Path(os.environ["WINEPREFIX"] + "drive_c/windows" + system_dir + dll).exists():
                        external += dll + "\n"
                    elif Path(self.__exe_path.parents[0] / dll).exists():
                        internal += dll + "\n"
                
                # This was found in Cyberpunk 2077
                if "d3d12.dll" not in depends:
                    dirname = self.__exe_path.parents[0]
                    d3d11on12 = dirname / "d3d11on12.dll"
                    d3d11on7 = dirname / "d3d11on12.dll"
                    if d3d11on12.exists() or d3d11on7.exists():
                        features += self.__features.get("d3d12.dll") + "\n"
                            
                print(dependencies + "\n" + features + "\n" + external + "\n" + internal + "\n")
        else:
            print("booze: Unable to extract information to report")
