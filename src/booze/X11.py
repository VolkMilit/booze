from pathlib import Path
from Xlib import display

from .pelib import PElib
import xbu

def icon_cache(exe_path):
    try:
        Path(Path.home() / Path(".cache/booze/icon/")).mkdir(mode=0o777, parents=True, exist_ok=False)
    except Exception:
        pass

    exe_path = Path(exe_path)
    icon_path = Path.home() / Path(".cache/booze/icon/" + exe_path.stem + ".png")
    
    if icon_path.exists():
        return str(icon_path)
    
    # Convert to absolute path if not already
    if not exe_path.is_absolute():
        exe_path = Path.cwd() / exe_path
    
    pelib = PElib(str(exe_path))
    pelib.extract_icon()
    
    if icon_path.exists():
        return str(icon_path)
    
    # The default Booze icon
    return "/usr/share/icons/hicolor/300x300/apps/booze.png"

def get_name(exe_path):    
    pelib = PElib(exe_path)
    return pelib.name()

def set_window_icon_name(window_name, window_new_name, icon_path):
    ret = xbu.set_window_icon_name(window_name, window_new_name, icon_path)
        
    if ret[1] != "":
        print("booze: [{}] {}".format(*ret))
        
def resolution():
    return display.Display().screen().root.get_geometry()

def resolution_string():
    dsp = display.Display().screen().root.get_geometry()
    return str(dsp.width) + "x" + str(dsp.height)
