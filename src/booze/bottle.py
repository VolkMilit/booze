import os
import getpass
import subprocess

from . import (
    regedit,
    utils,
    wine
)

# Return arch of EXISTING bottle, keep out of CreateBottle object,
# there is couple of places where we don't need CreateBottle object
# but need to know bottle arch
def arch(WINEPREFIX) -> str:
    return os.path.exists(WINEPREFIX + "/drive_c/windows/syswow64") and "64" or "32"

# Try to detect current bottle arch
def current_arch() -> str:
    return os.path.exists(os.environ["WINEPREFIX"] + "/drive_c/windows/syswow64") and "64" or "32"

def go_to(path: str) -> None:   
    paths = ["prefix", "mounts", "C:", "windows", "system", "syswow", "users", "home", "appdata", "appsdata"]
    
    if path not in paths:
        print("booze: Unknown path, available are: " + ', '.join(paths))
        return
    
    real_path = os.environ["WINEPREFIX"]
    
    if path == "mounts":
        real_path = real_path + "/dosdevices"
    elif path == "C:":
        real_path = real_path + "/drive_c"
    elif path == "windows":
        real_path = real_path + "/drive_c/windows"
    elif path == "system":
        real_path = real_path + "/drive_c/windows/system32"
    elif path == "syswow":
        real_path = real_path + "/drive_c/windows/syswow64"
    elif path == "users":
        real_path = real_path + "/drive_c/users"
    elif path == "home":
        from .config import Config
        config = Config()
        real_path = config.fake_home()
    elif path == "appdata":
        real_path = real_path + "/drive_c/users/" + os.path.expanduser("~") + "/AppData"
    elif path == "appsdata":
        real_path = real_path + "/drive_c/users/" + os.path.expanduser("~") + "/Application Data"
    
    if os.path.exists(real_path):
        subprocess.run(["xdg-open", real_path])
    else:
        print("booze: No such file or directory!")

class CreateBottle:
    def __init__(self, bottle_name: str, config):
        self.__bottle_name = bottle_name
        self.__fake_home = config.fake_home()
        self.__games_dirs = config.games_dirs()
        self.__prefix = config.prefixes_dir() + "/" + self.__bottle_name
        self.__packages_to_install = config.packages_to_install()
        self.__arch = os.environ.get("WINEARCH") or "win64"
        
        os.environ["WINEPREFIX"] = self.__prefix
        
        self.__create_new()
    
    def __sandbox(self):
        try:
            os.unlink(self.__prefix + "/dosdevices/z:")
        except Exception:
            print("{} doesn't have z: device, already sandboxed?".format(self.__bottle_name))
        
        try:
            abc = ["d", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
            abc = [i for i in abc if not os.path.islink(self.__prefix + "/dosdevices/{}:".format(i))]
            
            for i in range(0, len(self.__games_dirs)):
                d = self.__games_dirs[i]
                if d != '':
                    os.symlink(d, self.__prefix + "/dosdevices/{}:".format(abc[i]))
        except Exception as e:
            print(e)
        
        user = getpass.getuser()
        user_dir = self.__prefix + "/drive_c/users/{}/".format(user)
            
        # Make sure we don't have links to any of this directory, so programs like PZlib
        # think we're using EN_gb locale and games like The Surge will save game properly
        listdirs = ("Мои документы", "Мои рисунки", "Мои фильмы", "Моя музыка", "Рабочий стол")
        listdirs_fake = ("Personal", "My Pictures", "My Videos", "My Music", "Desktop")
            
        if not os.path.islink(user_dir + "/Saved Games"):
            os.rmdir(user_dir + "/Saved Games")
            os.symlink(self.__fake_home, user_dir + "/Saved Games")
            
        for dr in listdirs:
            utils.delete_if_exist(user_dir + dr)
            
        for dr in listdirs_fake:
            utils.delete_if_exist(user_dir + dr)
                
            os.symlink(self.__fake_home, user_dir + dr)
            regedit.add("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders", 
                                dr, "C:\\users\\{}\\{}".format(user, dr))
            regedit.add("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders",    
                    dr, "%USERPROFILE%\\{}".format(dr))
            
        utils.delete_if_exist(self.__prefix + "/drive_c/users/Public/Documents")
        os.symlink(self.__fake_home, self.__prefix + "/drive_c/users/Public/Documents")
        
        # Not sandbox, but just set desktop to black by default
        regedit.add("HKEY_CURRENT_USER\\Control Panel\\Colors", "Background", "0 0 0")

    # Note: syswow64 and system32 reverted for this binaries,
    # And this is correct behaviour, thanks to M$
    # see: https://github.com/doitsujin/dxvk/issues/1244
    # for reference
    def __install_DXVK(self):
        if not os.path.exists("/usr/share/dxvk"):
            print("Skip install of DXVK, as it not installed, or not installed properly. Please make sure you have at least /usr/share/dxvk directory.")
            return
        
        print("Installing DXVK...")
        d = "{}/drive_c/windows".format(self.__prefix)
        libslist = ("d3d10core", "d3d11", "dxgi")
        
        for lib in libslist:
            if self.__arch == "win64":
                os.rename("{}/syswow64/{}.dll".format(d, lib), "{}/syswow64/{}-original.dll".format(d, lib))
                os.symlink("/usr/share/dxvk/x32/{}.dll".format(lib), "{}/syswow64/{}.dll".format(d, lib))
                
            os.rename("{}/system32/{}.dll".format(d, lib), "{}/system32/{}-original.dll".format(d, lib))
            os.symlink("/usr/share/dxvk/x64/{}.dll".format(lib), "{}/system32/{}.dll".format(d, lib))
            regedit.add("HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides", lib, "native")
            
        if self.__arch == "win64":
            os.rename("{}/syswow64/d3d9.dll".format(d), "{}/syswow64/d3d9-original.dll".format(d))
            os.symlink("/usr/share/dxvk/x32/d3d9.dll", "{}/syswow64/d3d9.dll".format(d))
            
        os.rename("{}/system32/d3d9.dll".format(d), "{}/system32/d3d9-original.dll".format(d))
        os.symlink("/usr/share/dxvk/x64/d3d9.dll", "{}/system32/d3d9.dll".format(d))
        regedit.add("HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides", "d3d9", "native")
        
        # See https://github.com/Joshua-Ashton/d9vk/issues/200
        self.__install_libs([
            "d3dcompiler_43",
            "d3dcompiler_47",
            "d3dx9"
        ])
        
    def __install_vkd3d(self):
        vkd3d_path = "/usr/share/vkd3d"

        if not os.path.exists(vkd3d_path):
            vkd3d_path = "/usr/share/vkd3d-proton"
        
        if not os.path.exists(vkd3d_path):
            print("WARNING: Skip installation of vkd3d as it not installed or not installed properly. Please make sure you have at least /usr/share/vkd3d(-proton) directory. Alternativly you can download vkd3d (dll version) from here: https://github.com/HansKristian-Work/vkd3d-proton/releases . Put x86 version in {}/c_drive/windows/syswow64 and x64 version {}/c_drive/windows/system32 . No, this is not mistake, thanks M$ for that.".format(self.__prefix, self.__prefix))
            return
        
        print("Installing VKD3D...")
        d = "{}/drive_c/windows".format(self.__prefix)
        
        if self.__arch == "win64":
            os.rename(d + "/system32/d3d12.dll", d + "/system32/d3d12-original.dll")
            os.rename(d + "/system32/d3d12core.dll", d + "/system32/d3d12core-original.dll")
            os.symlink(vkd3d_path + "/x64/d3d12.dll", d + "/system32/d3d12.dll")
            os.symlink(vkd3d_path + "/x64/d3d12core.dll", d + "/system32/d3d12core.dll")
            
        os.rename(d + "/syswow64/d3d12.dll", d + "/syswow64/d3d12-original.dll")
        os.rename(d + "/syswow64/d3d12core.dll", d + "/syswow64/d3d12core-original.dll")
        os.symlink(vkd3d_path + "/x86/d3d12.dll", d + "/syswow64/d3d12.dll")
        os.symlink(vkd3d_path + "/x86/d3d12core.dll", d + "/syswow64/d3d12core.dll")

        regedit.add("HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides", "d3d12", "native")
        regedit.add("HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides", "d3d12core", "native")
        
    def __install_gallium_nine(self):
        if not os.path.exists("/usr/share/nine"):
            print("Skip installation of Gallium-Nine (Standalone), as it not installed, or not installed properly. Please make sure you have at least /usr/share/nine directory. Alternativly you can install standalone nine throught winetricks (booze -p {} install gallium-nine).".format(self.__bottle_name))
            return
        
        print("Installing Gallium Nine...")
        d = "{}/drive_c/windows".format(self.__prefix)
        
        if self.__arch == "win64":
            os.symlink("/usr/share/nine/lib64/d3d9-nine.dll.so", d + "/system32/d3d9-nine.dll")
            os.symlink("/usr/share/nine/lib64/ninewinecfg.exe.so", d + "/system32/ninewinecfg.exe")
        
        os.symlink("/usr/share/nine/lib32/d3d9-nine.dll.so", d + "/syswow64/d3d9-nine.dll")
        os.symlink("/usr/share/nine/lib32/ninewinecfg.exe.so", d + "/syswow64/ninewinecfg.exe")
        # We will not enable Gallium by default for all games, instead, we will enable it to specific games in profiles
        # because not all games works with Gallium, but it requires for games such as Dead Space, which has
        # very low fps with ether wined3d and d9vk (doesn't tested with new DXVK tho, Dark Souls, recently, was fixes)

    def __install_nvngx(self):
        if os.path.exists("/usr/lib/nvidia/wine/_nvngx.dll") and os.path.exists("/usr/lib/nvidia/wine/nvngx.dll"):
            print("Installing NVNGX...")
            os.symlink("/usr/lib/nvidia/wine/_nvngx.dll", self.__prefix + "/drive_c/windows/system32/_nvngx.dll")
            os.symlink("/usr/lib/nvidia/wine/nvngx.dll", self.__prefix + "/drive_c/windows/system32/nvngx.dll")
            regedit.add("HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides", "_nvngx", "native")
            regedit.add("HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides", "nvngx", "native")
    
    def __install_libs(self, libs):
        if libs is None or len(libs) == 0:
            return
                
        wine.launch_winetricks(libs, False)
        
    def __create_new(self):
        if os.path.exists(self.__prefix):
            print("booze: {} already exist in your installation.".format(self.__bottle_name))
            return
        
        # Prevent to install Gecko and Mono, also prevent building menus entries            
        os.environ["WINEDLLOVERRIDES"] = "mscoree,mshtml=d;winemenubuilder.exe=d;"
        
        # Initialize new wine bottle
        os.spawnvp(os.P_WAIT, "/usr/bin/wineboot", ["/usr/bin/wineboot", "-i"])
        
        print("Install additional libraries...")
        self.__sandbox()
        self.__install_DXVK()
        self.__install_vkd3d()
        self.__install_nvngx()
        self.__install_libs(self.__packages_to_install)
