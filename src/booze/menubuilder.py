from pathlib import Path

from xdg import (
    BaseDirectory,
    DesktopEntry
)

from . import X11
from .pelib import PElib
from .config import Config

def build_menu_entry(exe_path):
    exe_path = Path(exe_path)
    pe = PElib(str(exe_path))
    name = pe.name()
    prefix = ""
        
    if name == exe_path.name:
        name = input("Can't get name from application. What are desirable name for this game?: ")
            
    prefix = str(input("What prefix do you want to use for this game? (leave blank to use default): "))
        
    if not prefix:
        prefix = Config().default_prefix_name()
        
    icon = X11.icon_cache(str(exe_path))
        
    # Convert to absolute path if needed
    if exe_path.absolute():
        exe_path = Path.cwd() / exe_path
        
    entry = DesktopEntry.DesktopEntry()
    entry.new(BaseDirectory.xdg_data_home + "/applications/{}.desktop".format(name))
    entry.set("Version", "1.0") # Make various KDE software happy
    entry.set("Encoding", "UTF-8")
    entry.set("Name", name)
    entry.set("Exec", "sh -c 'booze -p {} \"{}\"'".format(prefix, str(exe_path)))
    entry.set("Icon", icon)
    entry.set("Categories", "Game;")
    entry.write()
        
    print()
    print("Done! Now you can launch your game with icon.")
