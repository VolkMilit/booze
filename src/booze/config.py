import os
from configparser import ConfigParser

from . import (
    paths,
    utils
)

class Config(ConfigParser):
    def __init__(self):
        # Well, shit. Python is mediocore when trying to subclass something.
        super().__init__()
        
        home = os.path.expanduser('~')
        
        self.__default = {
            "General": {
                "wine_prefixes_dir": home + "/wine_prefixes",
                "default_prefix": "default",
                "fake_home": home + "/wine_prefixes/user_files",
                "games_dirs": home + "/Games;",
                # Blacklist conhost.exe by default. Is this thing to emulating DOS?
                # I don't know, but it cause my CPU to eat 50% of all cores.
                # On Ryzen! WTF?
                "dll_overrides": "conhost.exe=d;",
                "packages_to_install": "",
                "wine_override": ""
            },
            
            "Hud": {
                # Since there is many Vulkan and DX12
                # games and both of them doesn't have
                # their own huds, we need to replace
                # it with something, like mango or vkmesa
                # on Mesa
                "default_hud": "mango",
                "dxvk": "fps",
                "gallium": "simple,fps",
                "vkmesa": "fps",
                "mango": "core_load"
            },
            
            "Overrides": {}
        }
        
        self.__booze_settings = paths.booze_settings
        
        if os.path.exists(self.__booze_settings):
            self.read(self.__booze_settings)
        else:
            print("booze: config file not found, creating default!")
            
            utils.try_mkdir(paths.booze_config)
            utils.try_mkdir(self.__default["General"]["wine_prefixes_dir"])
            utils.try_mkdir(self.__default["General"]["fake_home"])
            
            for section in self.__default.keys():
                self.add_section(section)
                
                for key, value in self.__default[section].items():
                    self.set(section, key, value)
            
            with open(self.__booze_settings, 'w') as f:
                self.write(f)
        
    def __get(self, section: str, key: str):
        return self.get(section, key, fallback=self.__default.get(section).get(key))
    
    def prefixes_dir(self):
        return self.__get("General", "wine_prefixes_dir")
    
    def default_prefix_path(self):
        return "{}/{}".format(
            self.__get("General", "wine_prefixes_dir"),
            self.__get("General", "default_prefix")
        )
    
    def default_prefix_name(self):
        return self.__get("General", "default_prefix")
        
    def fake_home(self):
        return self.__get("General", "fake_home")
    
    def games_dirs(self):
        ret = self.__get("General", "games_dirs")
        return [x.strip() for x in ret.split(';')]

    def packages_to_install(self):
        ret = self.__get("General", "packages_to_install")
        return [x.strip() for x in ret.split(';')]
    
    def dll_overrides(self):
        return self.__get("General", "dll_overrides")
    
    def wine_override(self):
        return self.__get("General", "wine_override") or "/usr/bin/"
    
    def hud_dxvk(self):
        return self.__get("Hud", "dxvk")
        
    def hud_gallium(self):
        return self.__get("Hud", "gallium")
        
    def hud_vkmesa(self):
        return self.__get("Hud", "vkmesa")
        
    def hud_mango(self):
        return self.__get("Hud", "mango")
    
    def default_hud(self):
        return self.__get("Hud", "default_hud")
    
    def set_exe_overrides(self, exe_name, proton_path):
        self.set("Overrides", exe_name, proton_path)
        
        with open(self.__booze_settings, 'w') as f:
            self.write(f)
    
    def exe_overrides(self, exe_name):
        return self.__get("Overrides", exe_name)
