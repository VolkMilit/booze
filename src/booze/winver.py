from . import (
    wine,
    bottle,
)

__valid_versions = ["win7", "win2003", "win2008", "win2008r2", "winxp64", "winxp", "nt351", "nt40", "vista", "win10", "win20", "win2k", "win30", "win31", "win7", "win8", "win81", "win95", "win98", "winme"]

def setwinver(ver, wine_exe) -> None:
    if ver is None:
        return
    
    # In winetricks it's win7, but most games relay on win10
    if ver == "default":
        ver = "win10"
    elif ver == "win2k3":
        ver = "win2003"
    elif ver == "win2k8":
        ver = "win2008"
    elif ver == "win2k8r2":
        ver = "win2008r2"
    
    if ver not in __valid_versions:
        print("booze: {} is not a valid windows version, skip.".format(ver))
        return
    
    # Don't know if this make difference, but I found this in winetricks
    if bottle.current_arch() == "64" and ver == "winxp":
        ver = "winxp64"
    
    wine.winecfg(["-v", ver], wine_exe)
