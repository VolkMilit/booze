import os

from . import utils
from .config import Config

__config = Config()
huds = ("dxvk", "vkmesa", "native", "mango", "none")

def __disable_hud():
    os.environ["DXVK_HUD"] = "0"
    os.environ["GALLIUM_HUD"] = "0"
    os.environ["__GL_SHOW_GRAPHICS_OSD"] = "0"
    os.environ["ENABLE_VK_LAYER_VALVE_steam_overlay_1"] = "1"

def __dxvk():
    os.environ["DXVK_HUD"] = __config.hud_dxvk()
    os.environ["GALLIUM_HUD"] = "0"
    os.environ["__GL_SHOW_GRAPHICS_OSD"] = "0"
    os.environ["ENABLE_VK_LAYER_VALVE_steam_overlay_1"] = "0"

def __native():
    os.environ["DXVK_HUD"] = "0"
    os.environ["ENABLE_VK_LAYER_VALVE_steam_overlay_1"] = "0"

    if utils.is_nvidia():
        os.environ["__GL_SHOW_GRAPHICS_OSD"] = "1"
    else: # amd and intel, also, nvidia *can* use thouse overlays, but we will
        # only use them for amd and intel
        os.environ["VK_INSTANCE_LAYERS"] = "VK_LAYER_MESA_overlay"
        os.environ["VK_LAYER_MESA_OVERLAY_CONFIG"] = __config.hud_vkmesa()
        os.environ["GALLIUM_HUD"] = __config.hud_gallium()
    
def __mango():
    os.environ["DXVK_HUD"] = "0"
    os.environ["MANGOHUD"] = "1"
    os.environ["GALLIUM_HUD"] = "0"
    os.environ["ENABLE_VK_LAYER_VALVE_steam_overlay_1"] = "0"
    os.environ["MANGOHUD_CONFIG"] = __config.hud_mango()
    
    winedlloverrides = os.environ.get("WINEDLLOVERRIDES") or []

    dxvk = "d3d11=b" in winedlloverrides
    d9vk = "d3d9=b" in winedlloverrides
        
    if dxvk or d9vk:
        if os.environ.get("LD_PRELOAD") is not None:
            os.environ["LD_PRELOAD"] += ":libMangoHud.so"
        else:
            os.environ["LD_PRELOAD"] = ":libMangoHud.so"

def __valve():
    os.environ["VK_INSTANCE_LAYERS"] = ""
    os.environ["DXVK_HUD"] = "0"
    os.environ["__GL_SHOW_GRAPHICS_OSD"] = "0"
    os.environ["ENABLE_VK_LAYER_VALVE_steam_overlay_1"] = "1"

def enable(args):
    if args == "none":
        __disable_hud()
    elif args == "valve":
        __valve()
    elif args == "native" or args == "vkmesa":
        __native()
    elif args == "mango":
        __mango()
    elif args == "dxvk":
        __dxvk()
    else:
        print("booze: invalid hud, choice from {}, skip this option.".format(', '.join(huds)))
        
def enable_default():
    enable(__config.default_hud())
