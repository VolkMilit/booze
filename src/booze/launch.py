import os
import sys

from .profiles import Profile
from . import (
    cache,
    wine,
    winver
)

def is_exe_exist(prefix, program):
    exe_name = os.path.basename(program)

    c_drive = prefix + "/drive_c/"
    windows = c_drive + "/windows/" + exe_name
    system32 = c_drive + "/windows/system32/" + exe_name
    syswow64 = c_drive + "/windows/syswow64/" + exe_name

    # Search everywhere in windows, system32, syswow64 and current path for exe
    if os.path.exists(program):
        return program
    elif os.path.exists(program + ".exe"):
        return program + ".exe"
    elif os.path.exists(program + ".EXE"):
        return program + ".EXE"
    elif os.path.isfile(windows):
        return windows
    elif os.path.isfile(windows + ".exe"):
        return windows + ".exe"
    elif os.path.isfile(system32):
        return system32
    elif os.path.isfile(system32 + ".exe"):
        return system32 + ".exe"
    elif os.path.isfile(syswow64):
        return syswow64
    elif os.path.isfile(syswow64 + ".EXE"):
        return syswow64 + ".EXE"
    else:
        print("booze: Can't execute '{}' no such file or directory.".format(program))
        sys.exit(-1)
        
def write_dxvk_config(path, content):
    if not content:
        return
    
    if not os.path.exists(path + "/dxvk.conf"):
        content_write = ""
        
        with open(path + "/dxvk.conf", 'w') as f:
            for key in content.keys():
                content_write += key + " = " + content[key]
                
            f.write(content_write)

def run(program = [], desktop = False, config = None) -> None:
    prefix = os.environ.get("WINEPREFIX")
    
    if prefix is None:
        print("booze: WINEPREFIX is empty!")
        os.exit(-1)

    dirname = os.getcwd()

    if os.environ.get("DXVK_ASYNC"):
        print("booze: DXVK async was enabled! DON'T use async in games with anticheat software! It can lead to you to be banned!")
    
    if "/" in program[0]:
        dirname = os.path.dirname(program[0])
        try:
            os.chdir(dirname)
        except Exception:
            print("booze: can't chdir to {}.".format(dirname))
            sys.exit(-1)
    
    exe_name = os.path.basename(program[0])
    # Check if exe path exists (e.g. user launch game without .exe)
    exe_name = is_exe_exist(prefix, dirname + "/" + exe_name)
    
    # Override all known dlls, if they was found in game directory
    # Explanation: some games prefer system libraries over ingame folder
    # see https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-loadlibraryexa
    # and WINE just do it, but Windows is more polish and will load
    # dll from folder, even if LoadLibraryExA is used.
    os.environ["WINEDLLOVERRIDES"] = os.environ.get("WINEDLLOVERRIDES") or ''
    os.environ["WINEDLLOVERRIDES"] += wine.get_overrides(dirname) + config.dll_overrides()
        
    # Create nvidia cache directory (this will improve performance)
    cache.create(exe_name, dirname)

    wine_override = config.exe_overrides(os.path.basename(exe_name)) or config.wine_override()
    
    profile = Profile(exe_name)
    if profile.have_item():
        overriden_exe = profile.override_exe()
        if overriden_exe:
            program[0] = overriden_exe
            
        program += profile.switches()
        desktop = profile.desktop()
        profile.esync()
        write_dxvk_config(dirname, profile.dxvk_data())
        winver.setwinver(profile.winver(), wine_override)
    
    wine.execv(program, exe_name.replace(" ", "_"), desktop, wine_override)
