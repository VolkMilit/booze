import subprocess

def add(path: str, key: str, value: str) -> None:
    subprocess.call(["/usr/bin/wine", "reg", "add", path, "/v", key, "/d", value, "/f"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    
def delete(path: str, key: str, value: str) -> None:
    subprocess.call(["/usr/bin/wine", "reg", "delete", path, "/v", key, "/f"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    
def set_file(f: str) -> None:
    subprocess.call(["/usr/bin/wine", "regedit", f], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

class Regedit:
    __reg = {
        "header": {}
    }
    __section = "header"
    __comment_num = 0
    __key = ""
    __value = ""
    __read_next = False
    __line_err = 0
    __FILE = None
    
    def __init__(self, path):
        self.__FILE = open(path, "r+")
        
        try:
            for line in self.__FILE.readlines():
                self.__line_err += 1
                
                if line == '\n':
                    continue
                
                line = line.strip()
                
                if self.__read_next:                
                    if line[-1:] == '\\':
                        line = line[:-1]
                    else:
                        self.__read_next = False
                    
                    self.__value += line
                    self.__reg[self.__section][self.__key] = self.__value
                    continue
                
                if line == "WINE REGISTRY Version 2\n":
                    continue
                
                # Section
                if line[0] == '[':            
                    section, time = line.split("] ")
                    self.__section = section[1:]               
                    
                    self.__reg[self.__section] = {
                        "time": time
                    }
                    
                if line[0] == '@' or line[0] == '"' or line[0] == '#':
                    try:
                        self.__key, self.__value = line.split('=')
                    except Exception:
                        self.__key = line
                        self.__value = None
                    
                    if self.__value and self.__value[-1:] == '\\':
                        self.__value = self.__value[:-1]
                        self.__read_next = True
                        
                    self.__reg[self.__section][self.__key] = self.__value
                
                # Comment
                if line[0] == ';':
                    self.__reg[self.__section]["$comment_" + str(self.__comment_num)] = line
                    self.__comment_num += 1
        except Exception:
            print("Error parsing file on line " + str(self.__line_err))
            
    def get(self, path, key):
        try:
            path = path.replace("\\", "\\\\")
            
            if key[0] != '#' and key != '@':
                key = '"' + key + '"'
                
            value = self.__reg[path][key]
                
            if value and value[0] == '"':
                value = value[1:-1]
            
            return value
        except Exception:
            return None
    
    def set(self, path, key, value):
        path = path.replace("\\", "\\\\")
        
        if self.__reg.get(path) is None:
            print("Writing new section is stab!")
            return
        
        if key[0] != '#' and key != '@':
            key = '"' + key + '"'
        
        self.__reg[path][key] = '"' + value + '"'
        
    def write(self):
        content = "WINE REGISTRY Version 2\n"
        
        # Header should be always first
        for key in self.__reg["header"]:
            if "$comment_" in key:
                content += self.__reg["header"][key] + "\n"
            else:
                content += key + "=" + self.__reg["header"][key] + "\n"
                
        content += "\n"
        
        for section in self.__reg.keys():
            if section == "header":
                continue
            
            content += "[" + section + "] " + self.__reg[section]["time"] + "\n"
            
            for key in self.__reg[section]:
                if key == "time":
                    continue
                
                if "$comment_" in key:
                    content += self.__reg[section][key] + "\n"
                elif self.__reg[section][key] is None:
                    content += key + "\n"
                else:
                    content += key + "=" + self.__reg[section][key] + "\n"
            
            content += "\n"
        
        self.__FILE.seek(0)
        self.__FILE.write(content)
    
    def close(self):
        self.__FILE.close()
