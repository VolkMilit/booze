import os
import subprocess
import getpass
import stat
import sys
import yaml
import hashlib
import re
import shutil
from termcolor import colored

from .bottle import CreateBottle
from .config import Config
from . import (
    regedit,
    menubuilder,
    X11,
    utils,
    wine
)

# Todo: check syntax

class Installer:
    def __init__(self, installer_path):
        if not os.path.isfile(installer_path):
            print("Invalid installer path. Abort.")
            sys.exit(-1)
        
        self.__config = Config()
        self.__exe_path = None
        self.__arch = "64"
        self.__installer_name = os.path.basename(installer_path).replace(".yml", "")
        self.__gamedir = self.__config.default_prefix_path()
        self.__files = {}
        self.__variables = {}
        
        os.environ["WINEPREFIX"] = self.__gamedir
        os.environ["WINEDEBUG"] = "-all"
        
        self.__variables["GAMEDIR"] = self.__gamedir
        self.__variables["PWD"] = os.getcwd()
        self.__variables["USERNAME"] = getpass.getuser()
        self.__variables["HOMEPATH"] = self.__gamedir + "/c_drive/users/" + getpass.getuser()
        self.__variables["APPDATA"] = self.__variables["HOMEPATH"] + "/AppData"
        self.__variables["LOCALAPPDATA"] = self.__variables["HOMEPATH"] + "/Local Settings/Application Data"
        self.__variables["APPLICATIONDATA"] = self.__variables["HOMEPATH"] + "/Application Data"
        self.__variables["CACHE"] = os.environ.get("XDG_CACHE_HOME") + "/booze"
        
        utils.try_mkdir(os.environ.get("XDG_CACHE_HOME") + "/booze")
        
        resolution = X11.resolution()
        self.__variables["RESOLUTION"] = X11.resolution_string()
        self.__variables["RESOLUTION_WIDTH"] = resolution.width
        self.__variables["RESOLUTION_HEIGHT"] = resolution.height
        
        content = ""
        with open(installer_path, 'r') as content_file:
            content = content_file.read()
            
        parse = yaml.safe_load(content)

        if parse.get("runner") and parse.get("runner") != "wine":
            print(colored("=> Only wine games is supported.", "red"))
            sys.exit(-1)
            
        if parse.get("script"):
            parse = parse["script"]

        for i in parse:
            item = (lambda x: parse[i])(i)
            
            # Since we're using profiles, system section should not be parsed
            
            if i == "files":
                self.__download_files(item)
            elif i == "game":
                self.__parse_game(item)
            elif i == "installer":
                self.__do_installer(item)
            elif i == "wine":
                self.__parse_wine(item)
            elif i == "require-binaries":
                self.__request_binaries(item)
        
        if os.path.islink(self.__gamedir + "/dosdevices/y:"):
            os.unlink(self.__gamedir + "/dosdevices/y:")
            
    def __create_tmp_symplinks(self):
        # TODO: dynamically search for letter availability
        if os.path.islink(self.__gamedir + "/dosdevices/y:"):
            os.unlink(self.__gamedir + "/dosdevices/y:")
            
        if os.path.islink(self.__gamedir + "/dosdevices/u:"):
            os.unlink(self.__gamedir + "/dosdevices/u:")
            
        os.symlink(os.getcwd(), self.__gamedir + "/dosdevices/y:")
        os.symlink(self.__variables["CACHE"], self.__gamedir + "/dosdevices/u:")
        
    def __cleanup(self):
        if os.path.islink(self.__gamedir + "/dosdevices/y:"):
            os.unlink(self.__gamedir + "/dosdevices/y:")
        
        if os.path.islink(self.__gamedir + "/dosdevices/u:"):
            os.unlink(self.__gamedir + "/dosdevices/u:")
            
    def __request_path(self, description, contains):
        path = str(input(description))
        
        if os.path.exists(path):
            if contains:
                if os.path.isfile(path + "/" + contains) and os.access(path, os.W_OK):
                    return path
            else:
                return path
        
        if contains:
            print("{} doesn't contains {} or not a valid path.".format(path, contains))
        else:
            print("{} is not valid.".format(path))
            
        return None
            
    # https://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
    def __binary_exist(self, program):
        def is_exe(fpath):
            return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

        fpath, fname = os.path.split(program)
        if fpath:
            if is_exe(program):
                return True
        else:
            for path in os.environ["PATH"].split(os.pathsep):
                exe_file = os.path.join(path, program)
                if is_exe(exe_file):
                    return True
                    
        return False
    
    # Not a best solution, but I could not come up with a better
    def __variable(self, s):
        if s is not None and "$" in s:
            for var, repl in self.__variables.items():
                s = s.replace("$" + var, repl)
        
        return s        
    
    def __download_files(self, files_array):
        for obj in files_array:
            file_alias = [*obj.keys()][0]
            file_real = [*obj.values()][0]
            file_name = os.path.basename(file_real)
            
            if type(file_real) is str:
                self.__files[file_alias] = file_name
                    
                if not os.path.exists(file_name):
                    utils.download(file_real, file_name)
            else:
                url = file_real["url"]
                filename = file_real["filename"]
                sha256 = file_real.get("sha256")
                                                        
                self.__files[file_alias] = filename
                
                if not os.path.exists(filename):
                    utils.download(url, filename)
                    
                    if sha256 is not None:
                        with open(filename, "rb") as f:
                            current_hash = hashlib.sha256(f.read()).hexdigest()
                            
                            if sha256 != current_hash:
                                print(colored("=> Hash of {} is not valid!".format(filename), "red"))
                    
        print("=> Downloading is complete.")

    def __request_binaries(self, binaries):
        bins = binaries.split(",")
        not_found = []
        
        for i in bins:
            bor = [f.strip() for f in i.split("|")]
            s = ""
            
            for b in bor:
                if s != "":
                    s += " or "
                
                if not self.__binary_exist(b):
                    s += b
            
            if s != "":
                not_found.append(s)
        
        if len(not_found) >= 1:
            print(colored("=> Required binaries not found: {}".format(', '.join(not_found)), "red"))
            sys.exit(-1)

    def __parse_game(self, game):
        if game.get("appid"):
            print(colored("=> Just what do you think you're doing, {}? {}, I really think I'm entitled to install an Steam game.".format(getpass.getuser(), getpass.getuser()), "red"))
            sys.exit(-1)
        
        self.__arch = game.get("arch").replace("win", "")
        self.__exe_path = game.get("exe")
        # Don't parse other value, this must be enough

    def __do_task(self, args):
        task_name = args.get("name")
        task_description = args.get("description")
        
        if task_description:
            print(colored("\t=> " + task_description, "green"))
            
        if task_name == "create_prefix":
            os.environ["WINEARCH"] = args.get("arch")
            
            CreateBottle(self.__installer_name, self.__config)
            
            self.__gamedir = self.__config.prefixes_dir() + "/" + self.__installer_name
            os.environ["WINEPREFIX"] = self.__gamedir
            self.__variables["GAMEDIR"] = self.__gamedir
        elif task_name == "winetricks":
            silent = args.get("silent") or False
            wine.launch_winetricks([args.get("app")], silent)
        elif task_name == "wineexec":
            executable = self.__variable(args.get("executable"))
            file_real_name = self.__files.get(executable)
            arg = self.__variable(args.get("args")) or ""
            os.environ["WINEPREFIX"] = self.__variable(args.get("prefix")) or self.__variables.get("GAMEDIR")
            
            self.__create_tmp_symplinks()
            
            if file_real_name is None:
                file_real_name = executable
            else:
                file_real_name = os.getcwd() + "/" + file_real_name
                
            args = [file_real_name]
                
            if arg != "":
                arg = re.split('"(.+?)"', arg)
                arg = [x.strip() for x in arg]
                args += arg

            wine.execv(args)
        elif task_name == "set_regedit":
            regedit.add(args.get("path"), args.get("key"), str(args.get("value")))
        elif task_name == "delete_registry_key":
            regedit.delete(args.get("path"), args.get("key"), str(args.get("value")))
        elif task_name == "winekill":
            subprocess.run(["wineboot", "-k"])

    # Convert nested dict to flat dict
    def __to_flat_dict(source_dict: dict) -> dict:
        ret = {}
        
        for i in source_dict:
            for name, value in i.items():
                ret[name] = value
                
        return ret

    def __remove(src: str) -> bool:
        if os.path.exists(src):
            print("=> {} Doesn't exist.".format(src))
            return False
        
        if os.path.isdir(src):
            shutil.rmtree(src)
        else:
            os.remove(src)
            
        return True

    def __do_installer(self, installer):
        for task in installer:
            for i in task:
                task_obj = task.get(i)
                
                if i == "task":
                    self.__do_task(task_obj)
                elif i == "merge" or i == "copy" or i == "move":
                    dst = self.__variable(task_obj.get("dst"))
                    src = self.__variable(task_obj.get("src"))
                    ok = True
                    
                    file_real_path = self.__files.get(src) or src
                    
                    if i == "merge" or i == "copy":
                        ok = utils.merge_files(file_real_path, dst, utils.merge_mode.COPY)
                    elif i == "move":
                        ok = utils.merge_files(file_real_path, dst, utils.merge_mode.MOVE)
                        
                    if not ok:
                        print("=> Copying one or more files are failed. Abort.")
                        sys.exit(-1)
                elif i == "remove":
                    src = task_obj.get("src")
                    self.__remove(src)
                elif i == "extract":
                    dst = self.__variable(task_obj.get("dst"))
                    file = task_obj.get("file")
                    file = self.__files.get(file) or self.__variable(file)
                    format = task_obj.get("format")
                    listfiles = task_obj.get("listfiles") or ''
                    
                    if listfiles != '':
                        listfiles = ' '.join(listfiles.split(';'))
                    
                    print("=> Extracting {}".format(self.__installer_name))
                    utils.extract(file, dst, format, listfiles)
                elif i == "chmodx":
                    task_obj = self.__variable(task_obj)
                    os.chmod(task_obj, stat.S_IREAD | stat.S_IRGRP | stat.S_IROTH | stat.S_IWRITE | stat.S_IEXEC | stat.S_IXGRP | stat.S_IXOTH)
                elif i == "input_menu":
                    description = task_obj.get("description")
                    options = self.__to_flat_dict(task_obj.get("options"))
                    preselect = task_obj.get("preselect")
                    id = task_obj.get("id")
                    self.__variables["INPUT_" + id] = utils.choice(options, description, preselect)
                elif i == "write_file":
                    file = self.__variable(task_obj.get("file"))
                    content = self.__variable(task_obj.get("content"))
                    mode = task_obj.get("mode") or 'w'
                    
                    with open(file, mode) as f:
                        f.write(content)
                elif i == "write_config":
                    file = task_obj.get("file")
                    data = task_obj.get("data")
                    merge = task_obj.get("merge") or True
                    
                    if data:
                        utils.write_config(file, data, merge)
                    else:
                        tmp = {}
                        tmp[task_obj.get("section")][task_obj.get("key")] = task_obj.get("value")
                        utils.write_config(file, tmp, merge)
                elif i == "write_json":
                    file = task_obj.get("file")
                    data = task_obj.get("data")
                    merge = task_obj.get("merge") or True
                    utils.write_json(file, data, merge)
                elif i == "execute":
                    args = self.__variable(task_obj.get("args"))
                    file = task_obj.get("file")
                    file = self.__files.get(file) or self.__variable(file)
                    command = task_obj.get("command")
                    
                    cmd = []
                        
                    if command:
                        cmd.append(command)
                    else:
                        cmd.append(file)
                        cmd.append(args)
                        
                    subprocess.run(cmd)
                elif i == "message":
                    print("=> " + self.__variable(task_obj))
                elif i == "request_install_path":                    
                    contains = task_obj.get("contains")
                    description = task_obj.get("description")
                    id = task_obj.get("id")
                    
                    path = None
                    
                    while not path:
                        path = self.__request_path(description, contains)
                        
                    self.__variables[id] = path
        
        if self.__exe_path:
            if "c_drive" in self.__exe_path and not os.path.exists(self.__exe_path):
                self.__exe_path = self.__gamedir + "/" + self.__exe_path
            
            menubuilder.build_menu_entry(self.__exe_path)
            
        self.__cleanup()
        
    def __parse_wine(self, wine):
        # Ignore all values of this section but overrides, as we must rely on profiles
        # Todo: We need to ignore some overrides, as already overriden by Booze
        
        overrides = wine.get("overrides")
        
        if overrides:
            for i in overrides:                
                regedit.add("HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides", i, overrides.get(i))
