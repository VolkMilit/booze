import os
import subprocess

from . import paths

def mount_iso(mount_point: str) -> str:    
    directory = paths.booze_cache + "/mount/" + os.path.basename(mount_point)
    
    if not os.path.exists(directory):
        os.mkdir(directory, 0o775)
    
    subprocess.run("fuseiso {} {}".format(mount_point, directory))
    
    return directory
    
def umount(directory: str) -> None:
    subprocess.run("fusermount -u {}".format(directory))
    
    # Remove only non-empty
    if len(os.listdir(directory)) == 0:
        os.rmdir(directory)
