from xdg import BaseDirectory

booze_config = BaseDirectory.xdg_config_home + "/booze"
booze_cache = BaseDirectory.xdg_cache_home + "/booze"
booze_data = BaseDirectory.xdg_data_home + "/booze"
booze_settings = booze_config + "/settings.cfg"
