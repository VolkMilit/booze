import os
import re
from configparser import ConfigParser

from . import profiles_defaults

class Profile:
    def __init__(self, path: str):
        self.__current_profile = None
        self.__dirpath = '/'.join(path.split('/')[:-1])
        self.__defaults = profiles_defaults.profiles_defaults
        
        name = os.path.basename(path)
        
        for profile_item in self.__defaults.keys():
            if profile_item[0] == '(':
                key_regexp = re.compile(profile_item, re.IGNORECASE)
                if key_regexp.search(name) is not None:
                    self.__current_profile = self.__defaults.get(profile_item)
            else:
                self.__current_profile = self.__defaults.get(name)
            
        if self.__current_profile is not None:
            self.__set_env()
            self.__set_dxvk_data()
                    
    def __set_env(self):
        environ = self.__current_profile.get("environ")
        if environ:
            for env, value in environ.items():
                if env == "WINEDLLOVERRIDES":
                    if os.environ.get("WINEDLLOVERRIDES") is not None:
                        os.environ[env] += value
                    else:
                        os.environ[env] = value
                else:
                    os.environ[env] = value
    
    def __set_dxvk_data(self):
        dxvk_file = self.__dirpath + "/dxvk.conf"
        data = self.__defaults.get("dxvk_data")
        
        if not os.path.exists(dxvk_file) and data:
            print("booze: write new dxvk data")
            parser = ConfigParser()
            parser.read_dict(data)
            
            with open(dxvk_file, "w") as dxvk_config:
                parser.write(dxvk_config)
        
    def desktop(self) -> bool:
        return self.__current_profile.get("force_desktop") or False
        
    def esync(self) -> None:
        esync = self.__current_profile.get("esync") or True
        if not esync:
            print("booze: esync was disabled")
            os.environ["WINEESYNC"] = "0"
            os.environ["WINEFSYNC"] = "0"
        
    def switches(self) -> list:
        return self.__current_profile.get("switches") or []
    
    def dxvk_data(self) -> dict:
        return self.__current_profile.get("dxvk_data")
    
    def override_exe(self) -> str:
        return self.__current_profile.get("override_exe") or None
    
    def winver(self) -> str:
        return self.__current_profile.get("winver") or None
    
    def have_item(self) -> bool:
        return self.__current_profile is not None
