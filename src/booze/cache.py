# import os
# import getpass
# import hashlib

def __is_nvidia():
    with open("/proc/bus/pci/devices", 'r') as dev:
        lines = dev.readlines()
            
        for line in lines:
            if "nvidia" in line:
                return True
    
    return False

def create(exe_name: str, exe_path: str):
    # # Temporary enable only for Nvidia, see https://gitlab.com/VolkMilit/booze/-/issues/47
    # if __is_nvidia():
    #     try:
    #         h = hashlib.md5(exe_path.encode("utf-8") + exe_name.encode("utf-8"))
    #         path = os.path.join("/home", getpass.getuser(), ".cache/booze", exe_name + '-' + h.hexdigest())
            
    #         # if not os.path.isdir(path):
    #         #     print("booze: can't find cache directory for {}, creating new...".format(exe_name))
    #         #     os.makedirs(path)
                
    #         # # Nvidia only, and only OpenGL
    #         # os.environ["__GL_SHADER_DISK_CACHE_PATH"] = path
    #     except Exception:
    #         print("booze: Can't create cache file for " + exe_name)
        
    # # DXVK (doesn't work?)
    # # see https://gitlab.com/VolkMilit/booze/-/issues/47
    # #os.environ["DXVK_STATE_CACHE_PATH"] = path
    pass
