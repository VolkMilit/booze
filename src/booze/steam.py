from glob import glob
from os.path import exists
import collections
import getpass

import vdf

class Steam:
    def __init__(self):
        self.__homepath = "/home/{}".format(getpass.getuser())
        self.__protons = {}
        
        for d in self.__content_dirs():
            for proton in glob(d + "/*/proton"):
                if "Collaider" not in proton:
                    proton_dir = proton.replace("proton", '')
                        
                    # Newest proton versions using files instead of dist
                    if exists(proton_dir + "/files"):                   
                        self.__protons[proton_dir.split('/')[-2]] = proton_dir + "files/bin/"
                    elif exists(proton_dir + "/dist"):
                        self.__protons[proton_dir.split('/')[-2]] = proton_dir + "dist/bin/"

    def __content_dirs(self) -> list:
        dirs = []
        
        steam_config = None
        config_path = self.__homepath + "/.steam/steam/config/config.vdf"
        with open(config_path, 'r') as vdf_file:
            steam_config = vdf.parse(vdf_file, mapper=collections.OrderedDict)
        
        cfg = steam_config["InstallConfigStore"]["Software"]["Valve"]["Steam"]    
            
        dirs.append(self.__homepath + "/.steam/steam/steamapps/common")
        dirs.append(self.__homepath + "/.steam/steam/compatibilitytools.d")
        dirs.append("/usr/share/steam/compatibilitytools.d")
            
        for i in range(1, 100):
            folder = cfg.get("BaseInstallFolder_" + str(i))
            if folder is not None:
                if exists(folder + "/steamapps/common"):
                    dirs.append(folder + "/steamapps/common")
                    
                if exists(folder + "/compatibilitytools.d"):
                    dirs.append(folder + "/compatibilitytools.d")
            
        return dirs
    
    def proton_list(self) -> list:
        ret = []
        
        for item in self.__protons.keys():
            ret.append(item)
        
        return ret
                        
    def get_proton(self, proton_name) -> str:
        return self.__protons[proton_name]
