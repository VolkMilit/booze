profiles_defaults = {        
    # Original Morrowind engine
    "Morrowind.exe": {
        #"force_desktop": True,
        "environ": {
            "mesa_glthread": "1",
            "vblank_mode": "0"
        }
    },
        
    # Low FPS with esync/fsync enabled
    "SonicGenerations.exe": {
        "esync": False
    },
    
    # From forums, add some more FPS, can be different for newer versions
    "PathOfExile.exe": {
        "switches": [
            "--nologo",
            "-gc 1",
            "--waitforpreload"
        ],
        
        # Only patched DXVK, otherwise will have no effect
        "environ": {
            "DXVK_ASYNC": "1"
        }
    },
        
    "SWRepublicCommando.exe": {
        "force_desktop": True
    },
        
    # Artifacts without this options whatsoever
    # Just use Heroic games launcher, for God sake
    "EpicGamesLauncher.exe": {
        "switches": [
            "-SkipBuildPatchPrereq",
            "-opengl"
        ]
    },

    # Halo: The Master Chief Collection has very bad vsync
    # like you will have input-lag with it enabled
    # I disabled it, but X11 tear still occurs, but DXVK
    # is a saviour
    "(mcclauncher|mcc-win64-shipping)": {
        "dxvk_data": { 
            "dxgi.tearFree": "True" 
        },

        # There still (!) bug in AMDGPU
        # that prventing from playing normally in Halo 2 (missing textures)
        # and Halo 4 (it's just crashing)
        # This variable switch between AMDGPU-PRO and AMDGPU (OpenSource)
        "environ": { 
            "AMD_VULKAN_ICD": "AMDVLK"
        }
    },
    
    # Doesn't even helping to set to borderless window mode,
    # so just use desktop mode
    "(control_dx11|control_dx12)": {
        "force_desktop": True
    },

    "(Cyberpunk2077|REDprelauncher)": {
        "winver": "win10", # This is now needed for RTX support, DLSS and other Nvidia things

        "switches": [
            "--launcher-skip",
            "--intro-skip",
            "-skipStartScreen"
        ],

        # Thouse are most likely placebo
        "environ": {
            "VKD3D_CONFIG": "dxr11",
            "WINE_HIDE_NVIDIA_GPU": "0",
            "PROTON_HIDE_NVIDIA_GPU": "0",
            "PROTON_ENABLE_NVAPI": "1",
            "VKD3D_FEATURE_LEVEL": "12_1",
            "DXVK_FRAME_RATE": "120",
            "DXVK_ASYNC": "1",
            "WINE_FSR_ENABLED": "1",
            # Sound is choppy without it
            #"PULSE_LATENCY_MSEC": "60"
        }
    },

    "witcher3.exe": {
        "winver": "win10", # This is now needed for RTX support, DLSS and other Nvidia things

        "environ": {
            "VKD3D_CONFIG": "dxr11",
            "WINE_HIDE_NVIDIA_GPU": "0",
            "PROTON_HIDE_NVIDIA_GPU": "0",
            "PROTON_ENABLE_NVAPI": "1",
            "VKD3D_FEATURE_LEVEL": "12_1",
            "DXVK_ASYNC": "1",
            "WINE_FSR_ENABLED": "1",
        }
    },

    # This old game only support 1280x1024 (maximum)
    # but launches in fullscreen 800x600
    "SunnyRace.exe": {
        "force_desktop": True
    },

    # Even older then Sunny Race, don't think
    # it even support resolution more then
    # 1280x768
    "ER.EXE": {
        "force_desktop": True
    }
} 
