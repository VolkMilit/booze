from os import environ

# Gain maximum performance
environ["WINEDEBUG"] = "-all"
environ["DXVK_LOG_LEVEL"] = "none"
# Easy on that log spams errors in
# Cyberpunk on Nvidia cards
environ["VKD3D_DEBUG"] = "none"
# There is no reason to separete thouse
# FSync enables, if kernel support it,
# else ESync is enabled by default
environ["WINEESYNC"] = "1"
environ["WINEFSYNC"] = "1"

# Disable all of this to prevent fps drops on Nvidia
environ["__GL_THREADED_OPTIMIZATIONS"] = "0"
environ["__GL_SYNC_TO_VBLANK"] = "0"
environ["__GL_YIELD"] = ""

# It turns out, that Proton enabled this value long time ago. 
# I don't think it can dome some harm, since many Bethesda game requires it.
environ["WINE_LARGE_ADDRESS_AWARE"] = "1"
