import os
import re
import subprocess
from glob import glob
from pathlib import Path

from . import X11

def __build_program(wine_path, desktop, name):
    wine_exe = "wine64"
    
    if os.environ.get("WINEARCH") == "win32":
        wine_exe = "wine"
        
    wine_executable = wine_path + wine_exe
    
    print("booze: using wine " + wine_executable)
    
    program = [wine_executable]
    
    if desktop:
        desktopres = "/desktop={},{}".format(name, X11.resolution_string())
        program += ["explorer", desktopres]
    
    return program

def execv(args, name = "Undefined", desktop = False, wine_path = "/usr/bin/"):
    program = __build_program(wine_path, desktop, name)
    
    if desktop:
        app_icon = X11.icon_cache(args[0])
        app_name = X11.get_name(args[0])
        
        subprocess.run(program + args)
        X11.set_window_icon_name("{} - Wine desktop".format(name), app_name, app_icon)
    else:
        subprocess.run(program + args)

def winecfg(args, wine_path = "/usr/bin/"):
    wine_exe = "wine64"
    
    if os.environ.get("WINEARCH") == "win32":
        wine_exe = "wine"

    subprocess.run([wine_path + "/" + wine_exe] + args)
    
def launch_winetricks(packages, stdout):
    winetricks: list = ["winetricks"] + packages
    ret: int = 0
    
    if stdout:
        ret = subprocess.run(winetricks, stderr=subprocess.STDOUT)
    else:
        print("Installing {}".format(' '.join(packages)))
        ret = subprocess.run(winetricks, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    
    if ret == 1:
        print("booze: winetrcks return code 1. Either package not found or something went wrong.")
        
# Force override some dlls found in game folder,
# this is sutable for enb, sweetfx or, for example,
# dsfix, which is used dinput8 as injector. GTA SA
# used some of this as a mod loader, etc.
def get_overrides(dirpath):
    overrides = ""
        
    # I don't know if all of this will works with D9\DXVK, but here goes nothing
    searchfor = ("dinput8", "xinput1_3", "xinput9_1_0", "d3d8", "d3d9", "dxgi", "d3d11")
    
    for i in glob(dirpath + "/*"):
        basename = Path(i).stem
        if basename in searchfor:
            overrides += basename + "=n,b;"
        
    return overrides
    
# Kill wine soft way: search for wineserver process, parser /proc/[number]/environ
# and get WINEPREFIX string, using wineboot to kill wine
# This will works only if other processes in wine responce, if one of wine
# process or wineserver itself is zombie, there is no way to kill wine
# except of killing all processes one by one
def kill():
    environ = ""
    
    for pid in filter(re.compile(r'[0-9]+').match, os.listdir("/proc")):
        cmd = open("/proc/" + pid + "/cmdline", 'r')
        cmdline = cmd.read()
        cmd.close()
        
        if "wineserver" in cmdline:
            with open("/proc/" + pid + "/environ", 'r') as environ:
                environ = environ.read().split('\x00')
            
            break
                
    for env in environ:
        if "WINEPREFIX" in env:
            os.environ["WINEPREFIX"] = env.split('=')[1]
            break
            
    subprocess.run(["wineserver", "-k"])
