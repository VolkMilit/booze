import os
import sys
from glob import glob
from pathlib import Path
from urllib3 import PoolManager
from configparser import ConfigParser
import json
import shutil
from enum import IntEnum
import subprocess

def extract(archive_path: str, output_path: str, aformat = None, files = '') -> None:
    if aformat == "gog":
        subprocess.run(["innoextract", "-emgsp", "--no-gog-galaxy", "-d " + output_path, archive_path])
    else:
        subprocess.run(["7z", "x", files, "-bso0", "-o" + output_path, archive_path])

merge_mode = IntEnum("merge_mode", "COPY MOVE")
def copy_move(src: str, dst: str, mode: int) -> None:
    if mode == merge_mode.COPY:
        shutil.copyfile(src, dst)
    elif mode == merge_mode.MOVE:
        shutil.move(src, dst)

def merge_files(src: str, dst: str, mode: int) -> None:
    if not os.path.exists(src):
        print("=> {} doesn't exist!".format(src))
        return False
    
    Path(dst).mkdir(parents=True, exist_ok=True)
    
    if os.path.isdir(src):
        for i in glob(src + "/*"):
            if os.path.isdir(i):
                merge_files(i, dst + "/" + os.path.dirname(i), mode)
            else:
                copy_move(i, dst + "/" + os.path.basename(i), mode)
    else:
        copy_move(src, dst + "/" + os.path.basename(src), mode)            
        
    return True

# Merge mode by default
def write_config(file_path: str, dictinary: dict, merge: bool) -> None:
    config = ConfigParser()
    mode = "w"
    
    if os.path.exists(file_path) and merge:
        config.read(file_path)
        mode = "r+"
        
    f = open(file_path, mode)
    
    sections = config.sections()
    
    for section, obj in dictinary.items():
        if section not in sections:
            config.add_section(section)
            
        for name, value in obj.items():
            config.set(section, name, value)
    
    config.write(f)
    f.close()
    
# Merge mode by default
def write_json(file_path: str, dictinary: dict, merge: bool) -> None:
    f = None
    config = None
    
    if os.path.exists(file_path):
        f = open(file_path, "r+")
        config = json.loads(f.read())
    else:
        f = open(file_path, "w")
        
    if merge:
        config.update(dictinary)
    elif not merge:
        config = dictinary
    
    json.dump(config, f)
    
    f.close()

def download(path: str, f: str) -> None:
    http = PoolManager()
    r = http.request("GET", path, preload_content=False)
        
    if r.status == 200:
        length = 0
        lenght_current = 0
        
        if r.info().get("Content-Length") is not None:
            length = int(int(r.info().get("Content-Length")) / 1024) + 1
        else:
            length = -1
        
        with open(f, "wb") as out:
            while True:                
                if length > 0:
                    lenght_current += 0.125
                    sys.stdout.write("\r=> Downloading {}: {}/{} Kb".format(f, int(lenght_current), length))
                elif length < 0:
                    sys.stdout.write("\r=> Downloading {} (streaming)".format(f))
                    
                sys.stdout.flush()
                
                data = r.read(128)
                if not data:
                    break
                out.write(data)
    
    print()
    r.release_conn()
            
def delete_if_exist(path: str) -> None:
    if os.path.exists(path):
        if os.path.islink(path):
            os.unlink(path)
        elif os.path.isdir(path):
            os.rmdir(path)
        else:
            os.remove(path)
            
def try_mkdir(path: str) -> None:
    if not os.path.exists(path):
        os.mkdir(path)

def is_nvidia():
    proc = subprocess.Popen(['lspci'], stdout=subprocess.PIPE)
    for item in proc.communicate()[0].decode().split("\n"):
        if "VGA" in item:
            return item.find("NVIDIA") != -1