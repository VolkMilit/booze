from setuptools import setup, Extension
import sys

py_version = "#include <python{}.{}/Python.h>".format(
    sys.version_info.major, sys.version_info.minor
)

outc = ""
with open("xbu/xbu.c", "r+") as c:
    for line in c.readlines():
        if "#include <python" in line:
            outc += py_version + '\n'
        else:
            outc += line

    c.seek(0)
    c.write(outc)
    c.truncate()

setup(
    name="Booze",
    version="0.1",
    packages=['booze',],
    license="GPL v3.0",
    long_description="Booze is a tool to controll all your WINE bottles and run WINE with specific parametrs.",
    ext_modules = [
        Extension("xbu", 
            sources = ["xbu/xbu.c"],
            libraries = ["png", "X11"]
        )
    ]
)
