/*
 * Remark on why we need that.
 * The Wine Desktop can't set different icon, It's hardcoded in program, 
 * and we can't change it without patching wine, witch is painfully, especially 
 * now, when Booze support Steam and some other launchers, like 
 * Heroic Games Launcher, and can use different wine in different situations.
 * So I write this. How does it works? It's simply looking for window mapevent
 * in loop, looking for explorer.exe with desktop in name, than libpng loads
 * icon from file and write it as CARDINAL32 format (aka PPM but with alpha support).
 * Then Xlib will replace the property of working window (_NET_WM_ICON and WM_ICON).
 * Then the name of window will change, removing "Desktop - ".
 * It's not that complicated, if you ever working with X11 api before.
 */

#include <python3.12/Python.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <png.h>

typedef unsigned long int CARD32;

enum XbuError {
    FileNotOpenedError = -1,
    HeaderError,
    PngPtrError,
    RgbrError,
    LongJmpError,
    X11AtomError,
    X11ChangePropertyError,
    X11FlushError,
    X11DisplayError,
    XbuSuccess
};

// https://stackoverflow.com/questions/27910906/xlib-test-window-names
Window get_window_with_name(Display* display, Window window, const char* name)
{
    Window *children, dummy;
    unsigned int nchildren;
    char *window_name = NULL;

    // We found window first try! Yay us!
    if (XFetchName(display, window, &window_name))
    {
        int r = strcmp(name, window_name);
        XFree(window_name);
        
        if (r == 0)
            return window;
    }

    // Otherwize keep looking
    if (XQueryTree(display, window, &dummy, &dummy, &children, &nchildren) != 0)
    {
        Window w = 0;
    
        for (unsigned int i = 0; i < nchildren; i++) 
        {        
            w = get_window_with_name(display, children[i], name);
        
            // Found it, nailed it
            if (w != 0)
                break;
        }
    
        if (children)
            XFree(children);
    
        return w;
    }
    
    // Can't find anything, giving up
    return 0;
}

// Using:
// Abforce's gist https://gist.github.com/abforce/2a4dbdeb47d4e6bcaf79de38380a13b9
// https://github.com/glennrp/libpng/blob/libpng16/png.h
// https://st.suckless.org/patches/netwmicon/st-netwmicon-0.8.4.diff
// to understand how this shit is working. X11 api is complicated
// and poorly documented.
int load_icon(const char* filename, unsigned int* ndata, CARD32** data)
{
    png_byte color_type;
    png_bytep *row_pointers = NULL;
    png_structp png_ptr;
    png_infop info_ptr;
    
    int i, j = 0;
    int width, height = 0;

    /* open file and test for it being a png */
    FILE *fp = fopen(filename, "rb");
    if (!fp)
        return FileNotOpenedError;

    // 8 is the maximum size that can be checked
    char header[8];
    fread(header, 1, 8, fp);
    if (png_sig_cmp((unsigned char*)header, 0, 8))
       return HeaderError;

    /* initialize stuff */
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr)
        return PngPtrError;

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
        return PngPtrError;

    // It's unclear in documentation but libpng 
    // using longjmp (basically goto) to bypass errors
    // in try-catch style
    setjmp(png_jmpbuf(png_ptr));

    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, 8);

    png_read_info(png_ptr, info_ptr);

    width = png_get_image_width(png_ptr, info_ptr);
    height = png_get_image_height(png_ptr, info_ptr);

    // Only RGBA is supported, this is mandatory for
    // CARDINAL32 format, blame X11
    color_type = png_get_color_type(png_ptr, info_ptr);
    if(color_type != PNG_COLOR_TYPE_RGBA)
        return RgbrError;
    
    png_read_update_info(png_ptr, info_ptr);

    // Again, it's goto, but I will leave this code
    // because otherwize assersion will happend
    // and Python, probably, segfault or assert within
    if (setjmp(png_jmpbuf(png_ptr)))
        return LongJmpError;

    /* memory allocation */
    row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
    for (i = 0; i < height; ++i)
        row_pointers[i] = (png_byte*) malloc(png_get_rowbytes(png_ptr, info_ptr));

    png_read_image(png_ptr, row_pointers);
    fclose(fp);
    
    // Write new data, it's basically PPM, but
    // without header and can contains alpha
    (*ndata) = (width * height) + 2;
    (*data) = (CARD32*)calloc((width * height) + 2, (*ndata));
    
    int x = 0;
    // First two bytes - width and height, just like in PPM
    (*data)[x++] = width;
    (*data)[x++] = height;
    
    // And here when fun begins
    for (i = 0; i < height; ++i) 
    {
        png_byte* row = row_pointers[i];
    
        for (j = 0; j < width; ++j) 
        {
            png_byte* ptr = &(row[j * 4]);
            
            signed char* cols = (signed char*)&((*data)[x++]);
            
            // Bytes are reversed, aka BGRA - blame X11 for that
            cols[0] = (signed char)ptr[2];
            cols[1] = (signed char)ptr[1];
            cols[2] = (signed char)ptr[0];
            cols[3] = (signed char)ptr[3];
        }
    }

    /* clean up */
    for (i = 0; i < height; i += 1){
        free(row_pointers[i]);
    }
    free(row_pointers);
    
    return XbuSuccess;
}

int change_window_icon(Display *display, Window window, const char *icon_path)
{
    Atom property = XInternAtom(display, "_NET_WM_ICON", 0);

    if (!property)
        return X11AtomError;

    unsigned int nelements = 0;
    CARD32* data;

    int status = load_icon(icon_path, &nelements, &data);
    if (status != XbuSuccess)
        return status;

    int result = XChangeProperty(display, window, property, XA_CARDINAL, 32, PropModeReplace, 
        (unsigned char*)data, nelements);

    if(!result)
        return X11ChangePropertyError;

    result = XFlush(display);

    if(!result)
        return X11FlushError;
    
    return XbuSuccess;
}

// Adapted from libxdo (https://github.com/jordansissel/xdotool/blob/master/xdo.c#L369)
int change_window_name(Display *display, Window window, const char *name)
{
    int ret = 0;
    
    // Change the property
    ret = XChangeProperty(display, window, 
                            XInternAtom(display, "WM_NAME", False), 
                            XInternAtom(display, "STRING", False), 8, 
                            PropModeReplace, (unsigned char*)name, strlen(name));
    if (ret == 0)
        return X11ChangePropertyError;

    // Change _NET_<property> just in case for simpler NETWM compliance?
    ret = XChangeProperty(display, window, 
                            XInternAtom(display, "_NET_WM_NAME", False), 
                            XInternAtom(display, "STRING", False), 8, 
    PropModeReplace, (unsigned char*)name, strlen(name));
    
    return ret;
}

int set_window_icon_name(const char *window_name, const char *new_name, const char *icon)
{    
    Display* display = XOpenDisplay(NULL);
    
    if (!display)
        return X11DisplayError;
    
    int screen = DefaultScreen(display);
    XSynchronize(display, 1);
    XEvent ev;
    // Only accept newest windows
    XSelectInput(display, RootWindow(display, screen), StructureNotifyMask | SubstructureNotifyMask);
    
    Window wid = 0;
  
    // Wait for window name to map, change name and icon and break loop
    while (1)
    {
        XNextEvent(display, &ev);
        
        if (ev.type == MapNotify)
        {
            XMapEvent *mapevent = (XMapEvent*)&ev;
            
            wid = get_window_with_name(display, mapevent->window, window_name);
            
            if (wid != 0)
                break;
        }
    }
    
    int status = change_window_icon(display, wid, icon);
    if (status != XbuSuccess)
    {
        XCloseDisplay(display);
        return status;
    }
    
    status = change_window_name(display, wid, new_name);
    if (status != XbuSuccess)
    {
        XCloseDisplay(display);
        return status;
    }
    
    XCloseDisplay(display);
    
    return XbuSuccess;
}

const char *error_string(int num)
{
    switch(num)
    {
    case FileNotOpenedError:
        return "Can't load image! No such file or directory!";
    case HeaderError:
        return "PNG has invalid header!";
    case PngPtrError:
        return "Can't create libpng pointer!";
    case RgbrError:
        return "Image is not RGBA!";
    case LongJmpError:
        return "Can't set longjpm in libpng!";
    case X11AtomError:
        return "Can't setup X11 Atom!";
    case X11ChangePropertyError:
        return "Can't setup X11 Property!";
    case X11FlushError:
        return "Can't flush X11 display!";
    case X11DisplayError:
        return "Can't create X11 Display!";
    case XbuSuccess:
        return "";
    default:
        return "Unhandlend error occurs.";
    }
}

// Python module
static PyObject *python_set_window_icon_name(PyObject *self, PyObject *args)
{   
    const char *window_name, *new_name, *icon;
    
    if (!PyArg_ParseTuple(args, "sss", &window_name, &new_name, &icon))
        return Py_BuildValue("(is)", -1, "Invalid argument to use in xbu.set_window_name!");
    
    int ret = set_window_icon_name(window_name, new_name, icon);
    return Py_BuildValue("(is)", ret, error_string(ret));
}

static PyMethodDef myMethods[] = {
    { "set_window_icon_name", python_set_window_icon_name, METH_VARARGS, "Set icon and name to specific window name" },
    { NULL, NULL, 0, NULL }
};

static struct PyModuleDef myModule = {
    PyModuleDef_HEAD_INIT, "xbu", NULL, -1, myMethods
};

// Initializes our module using our above struct
PyMODINIT_FUNC PyInit_xbu(void)
{
    return PyModule_Create(&myModule);
}
